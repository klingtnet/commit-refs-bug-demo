#!/usr/bin/env bash

set -euo pipefail

REPO=klingtnet%2Fcommit-refs-bug-demo

COMMITS=$(curl -sf --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" https://gitlab.com/api/v4/projects/$REPO/repository/commits | jq --raw-output '.[].short_id')
for COMMIT in $COMMITS; do
    echo $COMMIT
    curl -sf --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" https://gitlab.com/api/v4/projects/$REPO/repository/commits/$COMMIT/refs | jq -c '.[] | select(.type == "tag") | .name'
done