# Minimal example demonstrating that tags are associated to the wrong commits

- [#322847](https://gitlab.com/gitlab-org/gitlab/-/issues/322847)

This API resource [`GET /projects/:id/repository/commits/:sha/refs`][1] will return tags for commits that are untagged ~~if the parent commit has more than one commit associated to it~~.


# Expected

```sh
$ for COMMIT in $(git log --abbrev-commit --format=%h); do echo $COMMIT; git tag --points-at $COMMIT; done
58c5973
0.0.1
16bc606
5fe3cf9
```

# Actual

```sh
./refs.sh
58c59739
"0.0.1"
16bc6063
"0.0.1"
5fe3cf9a
"0.0.1"
```


[1]: https://docs.gitlab.com/ee/api/commits.html#get-references-a-commit-is-pushed-to
